<?php //include('mustlog.php');  ?>
<!DOCTYPE html>
<!--[if IE 8 ]>    <html lang="ru" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="ru" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="ru" class="no-js"> <!--<![endif]-->
<title>ACCEL | JH ACCESS SOLUTIONS CENTRE | JAMES HARDIE</title>
<meta charset="UTF-8">
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/basex.css">

  <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/themes/base/jquery-ui.css" type="text/css" media="all" />
  <link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" />
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js" type="text/javascript"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="js/jquery.scrollTo-min.js"></script>
  <script src="js/our-jqueryx.js" type="text/javascript"></script>
  <script type="text/javascript" src="js/mainx.js"></script>
  <script type="text/javascript" src="js/modernizr.js"></script>
  <!--[if IE]>  <link rel="stylesheet" href="css/iex.css"><![endif]-->

<script type="text/JavaScript" src="/js/cloud-carousel.1.0.5.js"></script>

  <link rel="shortcut icon" href="favicon.ico">
  <link rel="icon" type="image/ico" href="favicon.ico">  
  <script type="text/javascript">
  <!--
  function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
  }
  function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
  var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
  if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
  }
  function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
	d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);
	}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
	//-->
	</script>
  <script type="text/javascript" src="/js/jquery.mousewheel.min.js"></script>
  <script type="text/javascript" src="/js/jScrollPane.js"></script>
  <link rel="stylesheet" type="text/css" media="all" href="/js/jScrollPane.css" />
  
  
<script>
$(document).ready(function(){
						   
	// This initialises carousels on the container elements specified, in this case, carousel1.
	$("#carousel1").CloudCarousel(		
		{			
			xPos: 404,
			yPos: 85,
			buttonLeft: $("#left-but"),
			buttonRight: $("#right-but"),
			altBox: $("#alt-text"),
			titleBox: $("#title-text"),
			bringToFront: true,
			speed: 0.1,
			xRadius:280,
			yRadius: 50,
			minScale:0.8,
			reflOpacity: 0.4,
			mouseWheel:true,
			reflHeight: 30
		}
	);
	
	$('.close').click(function() {
		$(this).parent().parent().css('top','-1000px');
		});
	$('.sc_altdes #carousel1 a').each(function() {
		var ftr = $(this).attr('id')
		$(this).click(function() {
		$('.sc_popups > div').css('top','-1000px');
	$('.sc_popups div.' + ftr).css('top','50px');
	});
	});
	$('.sc_alt_designs #sc_nav ul li.altdes ul li a').each(function() {
		var fstr = $(this).attr('class')
		$(this).click(function() {
		$('.sc_popups > div').css('top','-1000px');
	$('.sc_popups div.' + fstr).css('top','50px');
	});
	});
	
	$('.sc_popups .sc_body').jScrollPane({showArrows:true, scrollbarWidth: 14});
	

});

</script>
</head>

<body class="sc_alt_designs">
<center>

<div id="page">


<div id="wrapper2">
<img src="images/leftshadow.jpg" alt="" class="leftshadow" border="0" width="6" />
<img src="images/bottomshadow.jpg" alt="" class="bottomshadow" border="0" height="7" />
<img src="images/sc_alt_des_bg.jpg" alt="" class="sc_altdes_bg" border="0" width="810" />

  <!-- BEGIN #HEADER -->
  <header id="main-header">
   
    <?php include('panels/logo.php');  ?>
	<?php include('panels/memberlogin.php');  ?>
  </header>
  <!-- END #HEADER -->


  <div class="js-slide2">
    <div class="top-bl all-menu">
       <?php include('menus/topmenu.php');  ?>
    </div>
    
  </div>

<div class="mainwrappbig sc_main">
	<div class="sc_mainnav">
	<?php include('menus/sc_leftnav.php');  ?>
	</div>

  <div class="column-right sc_altdes" >
   <div id = "carousel1" style="width:810px; height:438px; overflow:scroll;">            
            <!-- All images with class of "cloudcarousel" will be turned into carousel items -->
            <!-- You can place links around these images -->
            <a href="#1" id="sc_ad_eaves"><img class = "cloudcarousel sc_ad_eaves" src="images/sc_altdes_eaves.png" alt=" - Explore custom eaves & soffit designs using James Hardie products " height="248" title="Eaves & Soffits" /></a>
            <a href="#1" id="sc_ad_finishes"><img class = "cloudcarousel sc_ad_finishes" src="images/sc_altdes_finishes.png" alt=" - Explore custom finishes designed for James Hardie products " height="248"  title="Finishes" /></a>
            <a href="#1" id="sc_ad_flooring"><img class = "cloudcarousel sc_ad_flooring" src="images/sc_altdes_flooring.png" alt=" - Explore custom flooring solutions designed for James Hardie products" height="248"  title="Flooring" /></a>
            <a href="#1" id="sc_ad_intlining"><img class = "cloudcarousel sc_ad_intlining" src="images/sc_altdes_intlin.png" alt=" - COMING SOON! " height="248"  title="Internal Lining" /></a>
            <a href="#1" id="sc_ad_extlining"><img class = "cloudcarousel sc_ad_extlining" src="images/sc_altdes_extlin.png" alt=" - Explore custom external cladding designs using James Hardie products " height="248"  title="External Cladding" /></a>
        </div>
		<!-- Define left and right buttons. -->
        <a href="#1" id="left-but"><img src="images/sc_ad_left.jpg" alt="" width="32" border="0" /></a>
        <a href="#1" id="right-but"><img src="images/sc_ad_right.jpg" alt="" width="32" border="0" /></a>
        
        <!-- Define elements to accept the alt and title text from the images. -->
        <div class="titlealt">
		<span id="title-text"></span>
        <span id="alt-text"></span>
		</div>
		<div class="sc_popups">
			<div class="sc_ad_eaves">
				<h2>Eaves & Soffits
				<a class="close" href="#1">Close</a>
				</h2>
				<div class="sc_body">
				<ul>
				<li>
				<img src="images/sc_ad_ejvls.jpg" height="66" alt="" border="0"  />
				<h3>Expressed Joint Versilux Lining Soffit</h3>
				<p>
				This technical supplement provides fixing instructions to create an  expressed soffit using James Hardies 9mm Versilux lining.
				</p>
				<a target="_blank"  href="/downloads/Technical%20Literature/Supplements%20and%20Bulletins/JH%20Accel%20Versilux%2018.pdf">
				Technical Supplement
				</a>
				<br clear="all"/>
				</li>
				<li>
				<img src="images/sc_ad_ecuesa.jpg" height="66" alt="" border="0"  />
				<h3>External Cladding used in an eaves & soffit application</h3>
				<p>
				This supplement outlines James Hardie external cladding products used in an eave application for: Scyon™ AXON™ cladding, Scyon™ MATRIX™ cladding, PrimeLine® cladding,  HardiePlank® cladding and PanelClad® TextureLine cladding.
				</p>
				<a target="_blank" href="/downloads/Technical%20Literature/Supplements%20and%20Bulletins/JH%20Accel%20Eaves%20Soffits32.pdf">
				Technical Supplement
				</a>
				<br clear="all"/>
				</li>
				</ul>
				</div>
				
				
			</div>
			
			
			
			<div class="sc_ad_finishes">
				<h2>Finishes
				<a class="close" href="#1">Close</a>
				</h2>
				<div class="sc_body">
				<ul>
				<li>
				<img src="images/sc_ad_fin1.jpg" height="66" alt="" border="0"  />
				<h3>Wattyl® stained recommendations for profiled James Hardie external cladding</h3>
				<p>
				This product specification by Wattyl outlines their recommendation on providing a stained finish on certain James Hardie external cladding products such as HardiePlank® Woodgrain and Rusticated weatherboard and PanelClad® TextureLine sheets. Contact Wattyl® for further information on  warranty, suitability of product and maintenance.
				</p>
				<a style="margin: 30px 0 0 0;" target="_blank"  href="/downloads/solution_centre/custom_designs/Wattyl%20Solagard%20Masonry%20Matt%20Weathergard%20Stain%20System.PDF">
				Product Specification
				</a>
				<br clear="all"/>
				</li>
				<li>
				<img src="images/sc_ad_fin1.jpg" height="66" alt="" border="0"  />
				<h3>Cabot's stained recommendations for James Hardie external cladding</h3>
				<p>
				This product specification outlines the Cabot's stained system on fibre cement. Refer to Cabot's for further information on  warranty, suitability of product and maintenance.
				</p>
				<a style="margin: 30px 0 0 0;" target="_blank" href="/downloads/solution_centre/custom_designs/Cabots_Timbercolour_&_Ext_Varnish_Stain_over_fc.pdf">
				Product Specification
				</a>
				<a style="margin: 70px 0 0 0;" target="_blank" href="/downloads/solution_centre/custom_designs/Cabots%20Maintenance_fc_coated_Timbercolour_&_EV_Stain.pdf">
				Maintenance Schedule
				</a>
				<br clear="all"/>
				</li>
				</ul>
				</div>
				
				
			</div>
			
			
			
			<div class="sc_ad_flooring">
				<h2>Flooring
				<a class="close" href="#1">Close</a>
				</h2>
				<div class="sc_body">
				<ul>
				<li>
				<img src="images/sc_ad_flooring1.jpg" height="86" alt="" border="0"  />
				<h3>Fixing timber strip flooring over Scyon™ Secura™ interior flooring</h3>
				<p>
				This technical supplement is designed to aid in the fixing of 19mm timber strip flooring to Scyon™ Secura™ interior flooring in an internal floor application.
				</p>
				<a style="margin: 30px 0 0 0;" target="_blank"  href="/downloads/Technical%20Literature/Supplements%20and%20Bulletins/JH%20Accel%20Secura%20Interior%20Flooring30.pdf">
				Technical Supplement
				</a>
				<br clear="all"/>
				</li>
				
				</ul>
				</div>
				
				
			</div>
			
			
			
			
			
			<div class="sc_ad_extlining">
				<h2>External Cladding
				<a class="close" href="#1">Close</a>
				</h2>
				<div class="sc_body">
				<ul>
				<li>
				<img src="images/sc_ad_ext1.jpg" height="66" alt="" border="0"  />
				<h3>Angled ExoTec® facade panel and fixing system</h3>
				<p>
				This technical supplement allows the ability of the James Hardie Facade systems to be installed at a 30º angle off north
				</p>
				<a target="_blank"  href="/downloads/Technical%20Literature/Supplements%20and%20Bulletins/JH%20Accel%20Exotec1.pdf">
				Technical Supplement
				</a>
				<br clear="all"/>
				</li>
				<li>
				<img src="images/sc_ad_ext2.jpg" height="66" alt="" border="0"  />
				<h3>Vertical ScyonTM StriaTM cladding</h3>
				<p>
				This technical supplement covers the installation of ScyonTM StriaTM cladding on a 90º vertical orientation.
				</p>
				<a target="_blank" href="/downloads/Technical%20Literature/Supplements%20and%20Bulletins/JH%20Accel%20Vertical%20Stria26.pdf">
				Technical Supplement
				</a>
				<br clear="all"/>
				</li>
				
				
				<li>
				<img src="images/sc_ad_ext4.jpg" height="66" alt="" border="0"  />
				<h3>Angled ScyonTM StriaTM cladding</h3>
				<p>
				This technical supplement covers the installation of ScyonTM StriaTM cladding on a vertical angle orientation.
				</p>
				<div class="sc_note">
				<strong>Note:</strong> Only for ScyonTM StriaTM cladding 325mm
				</div>
				<a target="_blank" href="/downloads/Technical%20Literature/Supplements%20and%20Bulletins/JH%20Accel%20Angled%20Stria23.pdf">
				Technical Supplement
				</a>
				<br clear="all"/>
				</li>
				
				
				<li>
				<img src="images/sc_ad_ext5.jpg" height="66" alt="" border="0"  />
				<h3>Horizontal ScyonTM AxonTM cladding</h3>
				<p>
				This technical supplement is designed to help in the installation of ScyonTM AxonTM cladding in a horizontal orientation.
				</p>
				<a target="_blank" href="/downloads/Technical%20Literature/Installation%20Manuals%20and%20Technical%20Specifications/JH%20Accel%20Horizontal%20Axon24.pdf">
				Technical Supplement
				</a>
				<a class="caddet" id="hsac" style="margin: 40px 0 0 0;"  href="#1">
				CAD Details
				</a>
				
				<br clear="all"/>
				</li>
				
				
				<li>
				<img src="images/sc_ad_ext6.jpg" height="66" alt="" border="0"  />
				<h3>Horizontal EasyLapTM panel</h3>
				<p>
				This technical supplement is designed to aid in the installation of EasyLapTM panels in a horizontal orientation.
				</p>
				<a target="_blank" href="/downloads/Technical%20Literature/Supplements%20and%20Bulletins/JH%20Accel%20Horizontal%20EasyLap25.pdf">
				Technical Supplement
				</a>
				<a class="caddet" id="help" style="margin: 40px 0 0 0;" href="#1">
				CAD Details
				</a>
				<br clear="all"/>
				</li>
				
				
				<li>
				<img src="images/sc_ad_ext7.jpg" height="66" alt="" border="0"  />
				<h3>Fixing James HardieTM external cladding over brick walls</h3>
				<p>
				This supplement helps the designer to install a variety of James Hardie external cladding products over a brick wall.
				</p>
				<a target="_blank" href="/downloads/Technical%20Literature/Supplements%20and%20Bulletins/JH%20Accel%20Masonry%20Walls28.pdf">
				Technical Supplement
				</a>
				<a class="caddet" id="fjhec" style="margin: 40px 0 0 0;" href="#1">
				CAD Details
				</a>
				<br clear="all"/>
				</li>
				
				
				<li>
				<img src="images/sc_ad_fin1.jpg" height="66" alt="" border="0"  />
				<h3>Wattyl® stained recommendations for profiled James Hardie external cladding</h3>
				<p>
				This product specification by Wattyl outlines their recommendation on providing a stained finish on certain James Hardie external cladding products such as HardiePlank® Woodgrain and Rusticated weatherboard and PanelClad® TextureLine sheets. Contact Wattyl® for further information on  warranty, suitability of product and maintenance.
				</p>
				<a  style="margin: 30px 0 0 0;" target="_blank" href="/downloads/solution_centre/custom_designs/Wattyl%20Solagard%20Masonry%20Matt%20Weathergard%20Stain%20System.PDF">
				Product Specification
				</a>
				<br clear="all"/>
				</li>
				
				
				<li>
				<img src="images/sc_ad_fin1.jpg" height="66" alt="" border="0"  />
				<h3>Cabot's stained recommendations for James Hardie external cladding</h3>
				<p>
				This product specification outlines the Cabot's stained system on fibre cement. Refer to Cabot's for further information on  warranty, suitability of product and maintenance.
				</p>
				<a  style="margin: 30px 0 0 0;" target="_blank" href="/downloads/solution_centre/custom_designs/Cabots_Timbercolour_&_Ext_Varnish_Stain_over_fc.pdf">
				Product Specification
				</a>
				<a  style="margin: 70px 0 0 0;" target="_blank" href="/downloads/solution_centre/custom_designs/Cabots%20Maintenance_fc_coated_Timbercolour_&_EV_Stain.pdf">
				Maintenance Schedule
				</a>
				<br clear="all"/>
				</li>
				
				
				<li>
				<img src="images/sc_ad_ext8.jpg" height="66" alt="" border="0"  />
				<h3>Request design guidance</h3>
				<p>
				Do you have a design that is not available?
Please contact us with your request for review
				</p>
				<a target="_blank" href="/design_guidance.php">
				Ask James Hardie
				</a>
				<br clear="all"/>
				</li>
				</ul>
				</div>
				
				
			</div>
			<div class="caddet hsac">
			</div>
			<div class="caddet help">
			</div>
			<div class="caddet fjhec">
			</div>
		</div>
  </div>
</div>

<footer id="home-footer">
    <?php include('menus/bottommenu.php');  ?>	
	<?php include('panels/footer.php');  ?>
</footer>
</div>
</div>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-17010052-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script src="calc/jquery.cookie.js"></script>
<script>
  $('#login').click(function(){
    //alert('login');
    if ($('#remember').attr('checked')) {

      $.cookie('email', null);
      $.cookie('pass', null);
      $.cookie('remember', null);
      var email = $('#email').attr("value");
      var pass = $('#pass').attr("value");
      // set cookies to expire in 14 days
      $.cookie('email', email, { expires: 14 });
      $.cookie('pass', pass, { expires: 14 });
      $.cookie('remember', true, { expires: 14 });
      //alert(email+pass+"set");
    } else {
// reset cookies
      $.cookie('email', null);
      $.cookie('pass', null);
      $.cookie('remember', null);
      //alert(email+pass+"notset");
    }
    //false;
  });
 

  var remember = $.cookie('remember');
  if ( remember == 'true' ) {
    var email = $.cookie('email');
    var pass = $.cookie('pass');
// autofill the fields
    $('#email').attr("value", email);
    $('#pass').attr("value", pass);
    //alert(email+pass+"notset");
    $('#remember').attr('checked',true);
  }

  $(function() {
    var table = $('.jScrollPaneContainer #files_layer > table');  
    if (table.length > 0) {
      var scrollPane = $(table).parent().parent();
      var tableHeight = $(table).height();
      var scrollPaneHeight = $(scrollPane).height();
      if (tableHeight < scrollPaneHeight) {
        $(scrollPane).css('height', tableHeight + 'px');
      }
      $(table).parent().css('top', '0');
    }
  })();
  </script>
 <script>
  $(document).ready(function(){
  $('.sc_altdes #carousel1 a').css('display','block');
  });
</script>

</center>

  <!--[if lt IE 9]>
    <script type="text/javascript" src="js/PIE.js"></script>
  <![endif]-->
</body>
</html>