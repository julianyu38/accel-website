


	  
	  
	  <nav id="sc_nav">
<img src="images/sc_topnav.png" class="sc_topnav" width="159">

	  <ul>
	 
        <li class="apps"><a href="#1">
          <span>Applications:</span>
		</a>
			<ul>
				<li><a href="/sc_bushfire_construction.php">Bushfire Construction</a></li>
				<li><a href="/sc_curved_walls_calculator.php">Curved Walls</a></li>
				<li><a href="/sc_fastener_selection_tool.php">Fastener Selector</a></li>
				<li><a href="/sc_fire_walls_one.php">Fire & Acoustically Rated Walls - One Way</a></li>
				<li><a href="/sc_fire_walls_two.php">Fire & Acoustically Rated Walls - Two Way</a></li>
				<li><a href="/sc_fire_floors.php">Fire & Acoustically Rated Floors</a></li>
				<li><a href="/sc_slab_edge.php">Slab Edge Options</a></li>
			</ul>
		</li>
		
		
        <li class="sust"><a href="#1">
          <span>Sustainability:</span>
		</a>
		<ul>
				<li><a href="#1">6 Star Construction</a></li>
				<?php if ($type != "distributor") : ?><li><a href="#1">Green Star Guide</a></li><?php endif; ?>
				<li><a href="#1">3.12 Energy Efficiency Residential Guide</a></li>
				<li><a href="#1">Section J Energy Efficiency Guide</a></li>
				<li><a href="sc_r_values_guide.php">R Values guide</a></li>
			</ul>
		</li>
        <li class="altdes"><a href="sc_alt_designs.php">
          <span>Alternative Designs:</span>
		</a>
		<ul>
				<li><a class="sc_ad_eaves" href="/sc_alt_designs.php?eaves=1">Eaves & Soffits</a></li>

				<li><a   class="sc_ad_finishes"href="/sc_alt_designs.php?flooring=1">Finishes</a></li>
				<li><a class="sc_ad_flooring"   href="/sc_alt_designs.php?intlining=1">Flooring</a></li>
				<li><a class="sc_ad_intlining" href="/sc_alt_designs.php?extlining=1">Internal lining</a></li>
								<li><a class="sc_ad_extlining"  href="/sc_alt_designs.php?finishes=1">External Cladding</a></li>
			</ul>
		</li> 
		
		
        
	
		

      </ul>
	<img src="images/sc_botnav.png" class="sc_botnav" width="159"> 
	<img src="images/sc_divnav.png" class="sc_divnav" width="14"> 
    </nav>