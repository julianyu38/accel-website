<nav>
	<div class="name">Select a James Hardie product</div>
	<select onChange="location.href= $(this).val();">
		<option selected disabled></option>
		<optgroup label="External Cladding">
			<option value="http://www.accel.com.au/accessories_stria.php">Scyon&trade; Stria&trade; cladding</option>
			<option value="http://www.accel.com.au/accessories_matrix.php">Scyon&trade; Matrix&trade; cladding</option>
			<option value="http://www.accel.com.au/accessories_axon.php">Scyon&trade; Axon&trade; cladding</option>
			<option value="http://www.accel.com.au/accessories_linea.php">Scyon&trade; Linea&trade; weatherboard</option>
			<option value="http://www.accel.com.au/accessories_easylap.php">EasyLap&trade; Panels</option>
			<option value="http://www.accel.com.au/accessories_hardieflex.php">HardieFlex&#174; sheet</option>
			<option value="http://www.accel.com.au/accessories_hardietex.php">HardieTex&#174; base sheet</option>
			<option value="http://www.accel.com.au/accessories_primeline.php">PrimeLine&#174; weatherboard</option>
			<option value="http://www.accel.com.au/accessories_hardieplank.php">HardiePlank&#174; weatherboard</option>
			<option value="http://www.accel.com.au/accessories_panelclad.php">PanelClad&#174; sheet</option>
		</optgroup>
		<optgroup label="Facade systems">
			<option value="http://www.accel.com.au/accessories_exotec.php">ExoTec&#174; facade panel and fixing system</option>
			<option value="http://www.accel.com.au/accessories_comtex.php">ComTex&#174; facade panel and fixing system</option>
		</optgroup>
		<optgroup label="Trims">
			<option value="http://www.accel.com.au/accessories_axent.php">Scyon&trade; Axent&trade; trim</option>
		</optgroup>
		<optgroup label="Eaves">
			<option value="http://www.accel.com.au/accessories_hardieflex_eaves.php">HardieFlex&#174; sheet</option>
			<option value="http://www.accel.com.au/accessories_villaboard_eaves.php">Villaboard&#174; lining</option>
			<option value="http://www.accel.com.au/accessories_versilux_eaves.php">Versilux&#174; lining</option>
			<option value="http://www.accel.com.au/accessories_hardiegroove_eaves.php">HardieGroove&#174; lining</option>
		</optgroup>
		<optgroup label="Internal lining">
			<option value="http://www.accel.com.au/accessories_villaboard.php">Villaboard&#174; lining</option>
			<option value="http://www.accel.com.au/accessories_versilux.php">Versilux&#174; lining</option>
			<option value="http://www.accel.com.au/accessories_pineridge.php">PineRidge&#174; lining</option>
			<option value="http://www.accel.com.au/accessories_hardiegroove.php">HardieGroove&#174; lining</option>
		</optgroup>
		<optgroup label="Flooring & Decking">
			<option value="http://www.accel.com.au/accessories_scyon_interior.php">Scyon&trade; Secura&trade; interior flooring</option>
			<option value="http://www.accel.com.au/accessories_scyon_exterior.php">Scyon&trade; Secura&trade; exterior flooring</option>
			<option value="http://www.accel.com.au/accessories_hardiepanel.php">HardiePanel&#174; compressed sheets</option>
		</optgroup>
		<optgroup label="Underlays">
			<option value="http://www.accel.com.au/accessories_ceramic.php">James Hardie&trade; ceramic tile underlay</option>
			<option value="http://www.accel.com.au/accessories_vinyl.php">James Hardie&trade; vinyl and cork underlay</option>
		</optgroup>
		<optgroup label="Fencing">
			<option value="http://www.accel.com.au/accessories_hardiefence.php">HardieFence&#174; Easylock&#174; system (WA only)</option>
		</optgroup>
		<optgroup label="Bracing">
			<option value="http://www.accel.com.au/accessories_hardiebrace.php">HardieBrace&trade; sheets</option>
		</optgroup>
		<optgroup label="Columns">
			<option value="http://www.accel.com.au/accessories_artista.php">Artista&trade; columns</option>
		</optgroup>
	</select>
</nav>