<?php //include('mustlog.php');  ?>
<!DOCTYPE html>
<!--[if IE 8 ]>    <html lang="ru" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="ru" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="ru" class="no-js"> <!--<![endif]-->
<title>ACCEL | JH ACCESS SOLUTIONS CENTRE | JAMES HARDIE</title>
<meta charset="UTF-8">
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/basex.css">

<!--
  <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/themes/base/jquery-ui.css" type="text/css" media="all" />
  <link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" />
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js" type="text/javascript"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="js/jquery.scrollTo-min.js"></script>
  <script src="js/our-jqueryx.js" type="text/javascript"></script>
  <script type="text/javascript" src="js/mainx.js"></script>
  <script type="text/javascript" src="js/modernizr.js"></script>
  -->

  <link rel="stylesheet" href="css/jquery-ui.css" type="text/css" media="all" />
  <link rel="stylesheet" href="css/ui.theme.css" type="text/css" media="all" />
  <script src="js/jquery.min.js" type="text/javascript"></script>
  <script src="js/jquery-ui.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="js/jquery.scrollTo-min.js"></script>
  <script src="js/our-jqueryx.js" type="text/javascript"></script>
  <script type="text/javascript" src="js/mainx.js"></script>
  <script type="text/javascript" src="js/modernizr.js"></script>


  <!--[if IE]>  <link rel="stylesheet" href="css/iex.css"><![endif]-->

  <!-- TABS -->
  <link type="text/css" href="css/jquery-ui-1.8.24.custom.css" rel="stylesheet">
  <link rel="stylesheet" href="css/cadassist.css">
  <link rel="stylesheet" href="css/r-values.css">
  <script type="text/javascript" src="js/jquery-ui-1.8.24.custom.min.js"></script>
  <script type="text/javascript" src=js/r-values.js></script>
  <script type="text/javascript">
      $(function() {
        $('#tabs').tabs();
      });
  </script>
  <!-- -->

  <link rel="shortcut icon" href="favicon.ico">
  <link rel="icon" type="image/ico" href="favicon.ico">  
  <script type="text/javascript">
  <!--
  function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
  }
  function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
  var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
  if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
  }
  function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
	d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);
	}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
	//-->
	</script>
  <script type="text/javascript" src="js/jquery.mousewheel.min.js"></script>
  <script type="text/javascript" src="js/jScrollPane.js"></script>
  <link rel="stylesheet" type="text/css" media="all" href="js/jScrollPane.css" />
</head>

<body>
<center>

<div id="page">


<div id="wrapper2">
<img src="images/leftshadow.jpg" alt="" class="leftshadow" border="0" width="6" />
<img src="images/bottomshadow.jpg" alt="" class="bottomshadow" border="0" height="7" />

  <!-- BEGIN #HEADER -->
  <header id="main-header">
   
    <?php include('panels/logo.php');  ?>
	<?php include('panels/memberlogin.php');  ?>
  </header>
  <!-- END #HEADER -->


  <div class="js-slide2">
    <div class="top-bl all-menu">
       <?php include('menus/topmenu.php');  ?>
    </div>
  </div>

<div class="mainwrappbig sc_main">
	<div class="sc_mainnav">
	<?php include('menus/sc_leftnav.php');  ?>
	</div>

  <div class="column-right" id="main-r-val">
    <img class="main-image" src="images/r-val-main.png" alt="R-value image" width="343" height="403">
    <a target="_blank" href="/downloads/Technical%20Literature/Supplements%20and%20Bulletins/JH%20Accel%20RTechnical21.pdf"  class="r-val-downbtn">
    	Download R-value <br />
        Technical Supplement
    </a>
      <div class="textareaWP">
          <h1>Total R-values</h1>
          <div class="yellowLabel">This information outlines the total R-values for <br />
          James Hardie<sup>TM</sup> external cladding and flooring products</div>
          <div class="aWrapper">
              <h3>Wall Systems:</h3>
              <ul class="r-vals">
                <li>
                	<a href="#pg1" data-transition="slidedown">
                    Cladding fixed direct to frame
                    <span class="btnview">View</span>
                    </a>
                </li>
                <li>
                	<a href="#pg2" data-transition="slidedown">
                    Cladding fixed to 35mm thick treated timber battens
                    <span class="btnview">View</span>
                    </a>
                </li>
                <li>
                	<a href="#pg3">
                    Scyon<sup>TM</sup> Matrix<sup>TM</sup> Cladding on Scyon<sup>TM</sup> cavity battens
                    <span class="btnview">View</span>
                    </a>
                </li>
                <li>
                	<a href="#pg4">
                    ExoTec<sup>®</sup> and ComTex<sup>®</sup> facade panels on top hats
                    <span class="btnview">View</span>
                    </a>
                </li>
                <li>
                	<a href="#pg5">
                    Reverse brick veneer
                    <span class="btnview">View</span>
                    </a>
                </li>
            </ul>
            <h3>Floor Systems:</h3>
            <ul class="r-vals">
                <li>
                	<a href="#pg6">
                    Light weight floor solutions
                    <span class="btnview">View</span>
                    </a>
                </li>
            </ul>
          </div>
      </div>
  </div>
	<div class="column-right" id="pg1">
		<div class="pg_title">
			<a href="#pg0" class="closebtn" >Close</a>
	        <h3 class="sub_title">Wall Systems</h3>
	        <div class="main_title">Total R-values</div>
		</div>
        <div class="yellowLabel">
        	<h3>External Cladding fixed direct to frame:</h3>
            Scyon<sup>TM</sup>, Stria<sup>TM</sup>, Axon<sup>TM</sup>
            and Linea<sup>TM</sup> weatherboard, EasyLap<sup>TM</sup>panel,
            HardieFlex<sup>TM</sup> and PanelClad<sup>®</sup> sheets, HardieTex<sup>TM</sup>system,
            HardiePlank<sup>TM</sup>and PrimeLine<sup>®</sup>weatherboard and the ComTex<sup>®</sup> facade panel.
        </div>
        <div class="leftimg" >
			<img src="images/r-val-sub-1.png" alt="R-value image" width="237" height="277">
		    <a href="#important_pg"  class="importantbtn">
		    	Important<br>
	    	 	Notes
		    </a>
		</div>
      <div class="tablearea">
          <div class="aWrapper">
          	<table border="0" cellpadding="0" cellspacing="1">
          <tr>
            <td width="70" rowspan="2">Frame<br>
              Width<br>
              (mm)</td>
            <td width="70" rowspan="2">Maximum<br>
              stud<br>
              spacing<br>
              (mm)</td>
            <td width="70" rowspan="2">Vapour<br>
        permeable<br>
        membrane</td>
            <td colspan="3"><strong>INSULATION</strong></td>
            <td rowspan="2" width="60">Total R value<br>
              (summer or winter)<br>
              m2.K/W</td>
          </tr>
          <tr>
            <td width="100">Single reflective<br>
            vapour permeable membrane</td>
            <td width="100">Double reflective<br>
              vapour permeable membrane</td>
            <td width="70" >Bulk<br>
              insulation</td>
          </tr>
		</table>
        <div class="scroll_view">
        	<h3>TIMBER FRAME</h3>
            <table border="0" cellpadding="0" cellspacing="1">
              <tr>
                <td width="70">70 or 90</td>
                <td width="70">600</td>
                <td width="70">√</td>
                <td width="100">&nbsp;</td>
                <td width="100">&nbsp;</td>
                <td width="70">&nbsp;</td>
                <td width="60">0.41</td>
              </tr>
              <tr>
                <td>70 or 90</td>
                <td>600</td>
                <td>&nbsp;</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>0.90</td>
              </tr>
              <tr>
                <td>70 or 90</td>
                <td>600</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>0.90</td>
              </tr>
              <tr>
                <td>70 or 90</td>
                <td>600</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>R 1.5</td>
                <td>1.72</td>
              </tr>
              <tr>
                <td>70 or 90</td>
                <td>600</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>R 2.0</td>
                <td>2.22</td>
              </tr>
              <tr>
                <td>70 or 90</td>
                <td>600</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>R 2.1*</td>
                <td>2.40</td>
              </tr>
              <tr>
                <td>90</td>
                <td>600</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>R 2.5*</td>
                <td>2.72</td>
              </tr>
              <tr>
                <td>90</td>
                <td>600</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>R 2.7*</td>
                <td>2.92</td>
              </tr>
              <tr>
                <td>120</td>
                <td>600</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>R 3.0</td>
                <td>3.22</td>
              </tr>
            </table>
        	<h3>METAL FRAME</h3>
            <p>When a thermal break is installed,
            the above timber frame total R values mat be used.<br>
            (Themal break must have a minimum value R=0.2 otherwise use the values below)<br>
            No Thermal break installed</p>
            <table border="0" cellpadding="0" cellspacing="1">
      <tr>
                <td width="70">90</td>
                <td width="70">450</td>
                <td width="70">√</td>
                <td width="100">&nbsp;</td>
                <td width="100">&nbsp;</td>
                <td width="70">R 2.0</td>
                <td width="60">1.50</td>
              </tr>
              <tr>
                <td>90</td>
                <td>600</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>R 2.0</td>
                <td>1.60</td>
              </tr>
              <tr>
                <td>90</td>
                <td>450</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>R 2.5</td>
                <td>1.70</td>
              </tr>
              <tr>
                <td>90</td>
                <td>600</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>R 2.5</td>
                <td>1.90</td>
              </tr>
            </table>
            <p>* Fletcher insulations sell a product called SonoBatts<sup>®</sup>
        call them on 1800 000 878 for more information.</p>
        	</div>
          </div>
      </div>
	</div>
	<div class="column-right" id="pg2">
		<div class="pg_title">
			<a href="#pg0" class="closebtn" >Close</a>
	        <h3 class="sub_title">Wall Systems</h3>
	        <div class="main_title">Total R-values</div>
		</div>
        <div class="yellowLabel">
        	<h3>External Cladding over 35mm treated Timber Batterns (on stud):</h3>
            Scyon<sup>TM</sup>, Stria<sup>TM</sup>, Axon<sup>TM</sup>
            and Linea<sup>TM</sup> weatherboard, EasyLap<sup>TM</sup>panel,
            HardieFlex<sup>TM</sup> and PanelClad<sup>®</sup> sheets, HardieTex<sup>TM</sup>system,
            HardiePlank<sup>TM</sup>and PrimeLine<sup>®</sup>weatherboard and the ComTex<sup>®</sup> facade panel and fixing system.
        </div>
        <div class="leftimg" >
			<img src="images/r-val-sub-2.png" alt="R-value image" width="237">
		    <a href="#important_pg"  class="importantbtn">
		    	Important<br>
	    	 	Notes
		    </a>
		</div>
      <div class="tablearea">
          <div class="aWrapper">
          	<table border="0" cellpadding="0" cellspacing="1">
          <tr>
            <td width="70" rowspan="2">Frame<br>
              Width<br>
              (mm)</td>
            <td width="70" rowspan="2">Maximum<br>
              stud<br>
              spacing<br>
              (mm)</td>
            <td width="70" rowspan="2">Vapour<br>
        permeable<br>
        membrane</td>
            <td colspan="3"><strong>INSULATION</strong></td>
            <td rowspan="2" width="70" >Total R value<br>
              (summer or winter)<br>
              m2.K/W</td>
          </tr>
          <tr>
            <td width="100">Single reflective<br>
            vapour permeable membrane</td>
            <td width="100">Double reflective<br>
              vapour permeable membrane</td>
            <td width="60" >Bulk<br>
              insulation</td>
          </tr>
		</table>
        <div class="scroll_view">
        	<h3>TIMBER FRAME</h3>
            <table border="0" cellpadding="0" cellspacing="1">
              <tr>
                <td width="70">70 or 90</td>
                <td width="70">600</td>
                <td width="70">√</td>
                <td width="100">&nbsp;</td>
                <td width="100">&nbsp;</td>
                <td width="60">&nbsp;</td>
                <td width="70">0.6</td>
              </tr>
              <tr>
                <td>70 or 90</td>
                <td>600</td>
                <td>&nbsp;</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>1.1</td>
              </tr>
              <tr>
                <td>70 or 90</td>
                <td>600</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>1.8</td>
              </tr>
              <tr>
                <td>70 or 90</td>
                <td>600</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>R 1.5</td>
                <td>1.9</td>
              </tr>
              <tr>
                <td>70 or 90</td>
                <td>600</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>R 2.0*</td>
                <td>2.4</td>
              </tr>
              <tr>
                <td>70 or 90</td>
                <td>600</td>
                <td>&nbsp;</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>R 2.0*</td>
                <td>2.94</td>
              </tr>
              <tr>
                <td>70 or 90</td>
                <td>600</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>R 2.1*</td>
                <td>2.5</td>
              </tr>
              <tr>
                <td>70 or 90</td>
                <td>600</td>
                <td>&nbsp;</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>R 2.1*</td>
                <td>3.0</td>
              </tr>
              <tr>
                <td>90</td>
                <td>600</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>R 2.5*</td>
                <td>2.9</td>
              </tr>
              <tr>
                <td>90</td>
                <td>600</td>
                <td>&nbsp;</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>R 2.5*</td>
                <td>3.44</td>
              </tr>
              <tr>
                <td>90</td>
                <td>600</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>R 2.7*</td>
                <td>3.0</td>
              </tr>
              <tr>
                <td>90</td>
                <td>600</td>
                <td>&nbsp;</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>R 2.7*</td>
                <td>3.6</td>
              </tr>
              <tr>
                <td>120</td>
                <td>600</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>R 3.0*</td>
                <td>3.4</td>
              </tr>
            </table>

            <p>* Fletcher insulations sell a product called SonoBatts<sup>®</sup>
        call them on 1800 000 878 for more information.</p>
        	<p>NOTE: Refer to James Hardie<sup>®</sup> installation manuals to determine whether James Hardie<sup>®</sup>
        	external cladding can be fixed to 35mm thick Treated Timber Battens.   
        	</p>
        	
        	</div>
          </div>
      </div>
	</div>
	<div class="column-right" id="pg3">
		<div class="pg_title">
			<a href="#pg0" class="closebtn" >Close</a>
	        <h3 class="sub_title">Wall Systems</h3>
	        <div class="main_title">Total R-values</div>
		</div>
        <div class="yellowLabel">
        	<h3>Scyon<sup>TM</sup> Matrix<sup>TM</sup> Cladding fixed to 19mm Scyon<sup>TM</sup> cavity trim.</h3>
        </div>
        <div class="leftimg" >
			<img src="images/r-val-sub-3.png" alt="R-value image" width="237">
		    <a href="#important_pg"  class="importantbtn">
		    	Important<br>
	    	 	Notes
		    </a>
		</div>
      <div class="tablearea">
          <div class="aWrapper">
          	<table border="0" cellpadding="0" cellspacing="1">
          <tr>
            <td width="70" rowspan="2">Frame<br>
              Width<br>
              (mm)</td>
            <td width="70" rowspan="2">Maximum<br>
              stud<br>
              spacing<br>
              (mm)</td>
            <td width="70" rowspan="2">Vapour<br>
        permeable<br>
        membrane</td>
            <td colspan="3"><strong>INSULATION</strong></td>
            <td rowspan="2" width="70">Total R value<br>
              (summer or winter)<br>
              m2.K/W</td>
          </tr>
          <tr>
            <td width="100">Single reflective<br>
            vapour permeable membrane</td>
            <td width="100">Double reflective<br>
              vapour permeable membrane</td>
            <td width="60" >Bulk<br>
              insulation</td>
          </tr>
		</table>
        <div class="scroll_view">
        	<h3>TIMBER FRAME</h3>
            <table border="0" cellpadding="0" cellspacing="1">
              <tr>
                <td width="70">70 or 90</td>
                <td width="70">600</td>
                <td width="70">√</td>
                <td width="100">&nbsp;</td>
                <td width="100">&nbsp;</td>
                <td width="60">&nbsp;</td>
                <td width="70">0.60</td>
              </tr>
              <tr>
                <td>70 or 90</td>
                <td>600</td>
                <td>&nbsp;</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>1.12</td>
              </tr>
              <tr>
                <td>70 or 90</td>
                <td>600</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>1.70</td>
              </tr>
              <tr>
                <td>70 or 90</td>
                <td>600</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>R 1.5</td>
                <td>1.91</td>
              </tr>
              <tr>
                <td>70 or 90</td>
                <td>600</td>
                <td>&nbsp;</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>R 1.5</td>
                <td>2.43</td>
              </tr>
              <tr>
                <td>70 or 90</td>
                <td>600</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>R 2.0</td>
                <td>2.41</td>
              </tr>
              <tr>
                <td>90</td>
                <td>600</td>
                <td>&nbsp;</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>R 2.0</td>
                <td>2.94</td>
              </tr>
              <tr>
                <td>90</td>
                <td>600</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>R 2.5</td>
                <td>2.91</td>
              </tr>
              <tr>
                <td>90</td>
                <td>600</td>
                <td>&nbsp;</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>R 2.5</td>
                <td>3.44</td>
              </tr>
              <tr>
                <td>120</td>
                <td>600</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>R 3.0</td>
                <td>3.41</td>
              </tr>
              <tr>
                <td>120</td>
                <td>600</td>
                <td>&nbsp;</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>R 3.0</td>
                <td>3.95</td>
              </tr>
            </table>
		  <h3>METAL FRAME</h3>
            <p>Scyon<sup>TM</sup> cavity trim fixed either on or off stud</p>
            <table border="0" cellpadding="0" cellspacing="1">
      <tr>
                <td width="70">90</td>
                <td width="70">600</td>
                <td width="70">√</td>
                <td width="100">&nbsp;</td>
                <td width="100">&nbsp;</td>
                <td width="60">R 2.0</td>
                <td width="70">1.50</td>
              </tr>
              <tr>
                <td>90</td>
                <td>600</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>R 2.5</td>
                <td>1.70</td>
              </tr>
          </table>
            <p>Scyon<sup>TM</sup> cavity trim fixed on stud</p>
            <table border="0" cellpadding="0" cellspacing="1">
              <tr>
                <td width="70">90</td>
                <td width="70">600</td>
                <td width="70">&nbsp;</td>
                <td width="100">√</td>
                <td width="100">&nbsp;</td>
                <td width="60">R 2.0</td>
                <td width="70">1.70</td>
              </tr>
              <tr>
                <td>90</td>
                <td>600</td>
                <td>&nbsp;</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>R 2.5</td>
                <td>2.00</td>
              </tr>
            </table>
            <p>Scyon<sup>TM</sup> cavity trim fixed on stud</p>
            <table border="0" cellpadding="0" cellspacing="1">
              <tr>
                <td width="70">90</td>
                <td width="70">600</td>
                <td width="70">&nbsp;</td>
                <td width="100">√</td>
                <td width="100">&nbsp;</td>
                <td width="60">R 2.0</td>
                <td width="70">2.00</td>
              </tr>
              <tr>
                <td>90</td>
                <td>600</td>
                <td>&nbsp;</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>R 2.5</td>
                <td>2.20</td>
              </tr>
            </table>
<p>&nbsp;</p>
        </div>
          	
          </div>
      </div>
	</div>
	<div class="column-right" id="pg4">
		<div class="pg_title">
			<a href="#pg0" class="closebtn" >Close</a>
	        <h3 class="sub_title">Wall Systems</h3>
	        <div class="main_title">Total R-values</div>
		</div>
        <div class="yellowLabel">
        	<h3>ExoTec<sup>®</sup> facade panel and ComTex<sup>®</sup> facade panel fixed to James Hardie 35mm steel top hats.</h3>
        </div>
        <div class="leftimg" >
			<img src="images/r-val-sub-4.png" alt="R-value image" width="237">
		    <a href="#important_pg"  class="importantbtn">
		    	Important<br>
	    	 	Notes
		    </a>
		</div>
      <div class="tablearea">
          <div class="aWrapper">
          	
<table border="0" cellpadding="0" cellspacing="1">
          <tr>
            <td width="70" rowspan="2">Frame<br>
              Width<br>
              (mm)</td>
            <td width="70" rowspan="2">Maximum<br>
              stud<br>
              spacing<br>
              (mm)</td>
            <td width="70" rowspan="2">Vapour<br>
        permeable<br>
        membrane</td>
            <td colspan="3"><strong>INSULATION</strong></td>
            <td rowspan="2" width="70">Total R value<br>
              (summer or winter)<br>
              m2.K/W</td>
          </tr>
          <tr>
            <td width="100">Single reflective<br>
            vapour permeable membrane</td>
            <td width="100">Double reflective<br>
              vapour permeable membrane</td>
            <td width="60" >Bulk<br>
              insulation</td>
          </tr>
		</table>
        <div class="scroll_view">
        	<h3>TIMBER FRAME</h3>
            <table border="0" cellpadding="0" cellspacing="1">
              <tr>
                <td width="70">70 or 90</td>
                <td width="70">600</td>
                <td width="70">√</td>
                <td width="100">&nbsp;</td>
                <td width="100">&nbsp;</td>
                <td width="60">&nbsp;</td>
                <td width="70">0.60</td>
              </tr>
              <tr>
                <td>70 or 90</td>
                <td>600</td>
                <td>&nbsp;</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>1.11</td>
              </tr>
              <tr>
                <td>70 or 90</td>
                <td>600</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>1.78</td>
              </tr>
              <tr>
                <td>70 or 90</td>
                <td>600</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>R 1.5</td>
                <td>1.92</td>
              </tr>
              <tr>
                <td>70 or 90</td>
                <td>600</td>
                <td>&nbsp;</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>R 1.5</td>
                <td>2.60</td>
              </tr>
              <tr>
                <td>90</td>
                <td>600</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>R 2.0</td>
                <td>2.42</td>
              </tr>
              <tr>
                <td>90</td>
                <td>600</td>
                <td>&nbsp;</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>R 2.0</td>
                <td>3.12</td>
              </tr>
              <tr>
                <td>90</td>
                <td>600</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>R 2.5</td>
                <td>2.92</td>
              </tr>
              <tr>
                <td>90</td>
                <td>600</td>
                <td>&nbsp;</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>R 2.5</td>
                <td>3.64</td>
              </tr>
              <tr>
                <td>120</td>
                <td>600</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>R 3.0</td>
                <td>3.42</td>
              </tr>
              <tr>
                <td>120</td>
                <td>600</td>
                <td>&nbsp;</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>R 3.0</td>
                <td>4.15</td>
              </tr>
            </table>
		  <h3>METAL FRAME</h3>
            <table border="0" cellpadding="0" cellspacing="1">
      <tr>
                <td width="70">90</td>
                <td width="70">600</td>
                <td width="70">√</td>
                <td width="100">&nbsp;</td>
                <td width="100">&nbsp;</td>
                <td width="60">R 2.0</td>
                <td width="70">1.60</td>
              </tr>
              <tr>
                <td>90</td>
                <td>600</td>
                <td>&nbsp;</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>R 2.0</td>
                <td>2.20</td>
              </tr>
              <tr>
                <td>90</td>
                <td>600</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td> R 2.5</td>
                <td>1.80</td>
              </tr>
              <tr>
                <td>90</td>
                <td>600</td>
                <td>&nbsp;</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>R 2.5</td>
                <td>2.40</td>
              </tr>
          </table>
          <p>&nbsp;</p>
        </div>
          	
          </div>
      </div>
	</div>
	
	<div class="column-right" id="pg5">
		<div class="pg_title">
			<a href="#pg0" class="closebtn" >Close</a>
	        <h3 class="sub_title">Wall Systems</h3>
	        <div class="main_title">Total R-values</div>
		</div>
        <div class="yellowLabel">
        	<h3>Reverse brick veneer configuration: </h3>
            Scyon<sup>TM</sup> Stria<sup>TM</sup>, Axon<sup>TM</sup> and 
        	Linea<sup>TM</sup> cladding, EasyLap<sup>TM</sup> panel, HardiFlex<sup>®</sup> and PanelClad<sup>®</sup> sheets, HardiTex<sup>®</sup> base sheets, HardiPlank<sup>®</sup> and PrimeLine<sup>®</sup> cladding and the ComTex<sup>®</sup> facade panel and fixing system.
        </div>
        <div class="leftimg" >
			<img src="images/r-val-sub-5.png" alt="R-value image" width="270">
		    <a href="#important_pg"  class="importantbtn">
		    	Important<br>
	    	 	Notes
		    </a>
		</div>
      <div class="tablearea">
          <div class="aWrapper">
          	<table border="0" cellpadding="0" cellspacing="1">
          <tr>
            <td width="70" rowspan="2">Frame<br>
              Width<br>
              (mm)</td>
            <td width="70" rowspan="2">Maximum<br>
              stud<br>
              spacing<br>
              (mm)</td>
            <td colspan="3"><strong>INSULATION</strong></td>
            <td rowspan="2" width="70">Total R value<br>
              (summer or winter)<br>
              m2.K/W</td>
          </tr>
          <tr>
            <td width="100">Single reflective<br>
            vapour permeable membrane</td>
            <td width="100">&nbsp;</td>
            <td width="60" >Bulk<br>
              insulation</td>
          </tr>
		</table>
        <div class="scroll_view">
       	  <h3>TIMBER FRAME</h3>
            <table border="0" cellpadding="0" cellspacing="1">
              <tr>
                <td width="70">90</td>
                <td width="70">600</td>
                <td width="100">√</td>
                <td width="100">&nbsp;</td>
                <td width="60">R 2.0</td>
                <td width="70">3.22</td>
              </tr>
              <tr>
                <td>90</td>
                <td>600</td>
                <td>√</td>
                <td>&nbsp;</td>
                <td>R 2.5</td>
                <td>3.74</td>
              </tr>
            </table>
		  <p>*Fletcher insulation sell a product called SonoBatts<sup>®</sup> call them on 1800 000 878 for more information.</p>
      </div>
          </div>
      </div>
	</div>

	<div class="column-right" id="pg6">
		<div class="pg_title">
			<a href="#pg0" class="closebtn" >Close</a>
	        <h3 class="sub_title">Floor Systems</h3>
	        <div class="main_title">Total R-values</div>
		</div>
        <div class="yellowLabel">
        	<h3>Flooring fixed direct to flooring joists: </h3>
        	Scyon<sup>TM</sup> Secura<sup>TM</sup> interior flooring &amp; 15mm Hardiepanel<sup>®</sup> compressed flooring (HPC)
        </div>
        <div class="leftimg" >
			<img src="images/r-val-sub-6.png" alt="R-value image" width="248">
		    <a href="#important_pg"  class="importantbtn">
		    	Important<br>
	    	 	Notes
		    </a>
		</div>
      <div class="tablearea">
          <div class="aWrapper">
          	<table border="0" cellpadding="0" cellspacing="1">
          <tr>
            <td><strong>PRODUCT</strong></td>
            <td colspan="2"><strong>SUBFLOOR</strong></td>
            <td colspan="2"><strong>INSULATION</strong></td>
            <td colspan="3"><strong>R VALUE</strong></td>
          </tr>
          <tr>
            <td width="70">JH Flooring<br>
            Product</td>
            <td width="74">Lining under<br>
            Floor joists</td>
            <td width="60">Ventilated</td>
            <td width="70">Reflective<br>
            sarking</td>
            <td width="60">Bulk<br>
              Insulation</td>
            <td width="60">R winter</td>
            <td width="60">R summer</td>
            <td width="60">R average</td>
          </tr>
		</table>
        <div class="scroll_view">
            <table border="0" cellpadding="0" cellspacing="1">
          <tr>
            <td width="70">15mm HPC / Scyon Secura 19mm</td>
            <td width="74">&nbsp;</td>
            <td width="60">&nbsp;</td>
            <td width="70">&nbsp;</td>
            <td width="60">&nbsp;</td>
            <td width="60">0.93</td>
            <td width="60">0.81</td>
            <td width="60">0.87</td>
          </tr>
          <tr>
            <td>15mm HPC / Scyon Secura 19mm</td>
            <td>4.5mm<br>
              HardieFlex<sup>TM</sup></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>1.16</td>
            <td>0.97</td>
            <td>1.07</td>
          </tr>
          <tr>
            <td>15mm HPC / Scyon Secura 19mm</td>
            <td>4.5mm<br>
HardieFlex<sup>TM</sup></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>R 1.5</td>
            <td>2.49</td>
            <td>2.3</td>
            <td>2.40</td>
          </tr>
          <tr>
            <td>15mm HPC / Scyon Secura 19mm</td>
            <td>4.5mm<br>
HardieFlex<sup>TM</sup></td>
            <td>√</td>
            <td>&nbsp;</td>
            <td>R 1.5</td>
            <td>1.79</td>
            <td>1.67</td>
            <td>1.73</td>
          </tr>
          <tr>
            <td>15mm HPC *</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Two layers top and bottom of floor joist (anti-glare)</td>
            <td>3.41</td>
            <td>1.43</td>
            <td>2.42</td>
          </tr>
          <tr>
            <td>15mm HPC / Scyon Secura 19mm</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>One layer btm only of floor joist</td>
            <td>2.13</td>
            <td>2.41</td>
            <td>2.27</td>
          </tr>              
            </table>
		  <h3>Note</h3>
		  <ol>
		  	<li>Systems
          marked with * can only have 15 mm HardiePanel<sup>TM</sup>
          compressed installed.
		  	</li>
		  	<li>Max. joist spacing is 450mm for all above flooring configurations.
		  	</li>
		  	<li>Tap is used for non ventilated airspace to stop dust. Airholes 5-6mm can be incorporated to help with condensation.
		  	</li>
		  	<li>The designer is responsable to identify and address moisture ingress issues including condensation build up in the floor space.
		  	</li>
		  </ol>
      </div>
          </div>
      </div>
	</div>

	<div class="column-right" id="pg_important">
		<div class="pg_title">
			<a href="#pg0" class="closebtn" >Close</a>
	        <h3 class="sub_title">Wall System Thermal Performance</h3>
	        <div class="main_title">Total R-values</div>
		</div>
        <div class="yellowLabel">
        	<h3>Important Note</h3>
        </div>
        <div class="content" >
			<ol start="1">
            <li>The above published Total R-Values for the above building system configurations were independently assessed in accordance with AS/NZS 4859.1 : 2002 'Materials' For The Thermal Insulation of Buildings', by certified engineers and industry organisations.</li>
            <li>For external cladding fixed to 35mm thick treated timber battens,Total R values for 'External Cladding Fixed to James Hardie 35mm steel top hats' on a timber frame may be used.<br>
                The specifier is respinsible to undertake specific design and detailing for areas outside the scope of James Hardie literature.</li>
            <li>Timber frame result are for thermal path only.</li>
            <li>Metal frame results take the effects of thermal bridging into account.</li>
            <li>Metal frames results are for 0.55mm BMT (k=50W/m.K) with a 35mm maximum flange.<br>
              For 0.8mm
            BMT steel frames, subtract 0.072 from total R value.<br>
            For 1.2mm
            BMT steel frames, subtract 0.133 from total R value.<br>
            For 1.6mm
            BMT steel frames, subtract 0.164 from total R value.<br>
            </li>
        </ol>
        <ol start="6">
            <li>If Villaboard lining or any other minimum 6mm thick James Hardie internal lining product is used for the internal layer, 0.04 must be subtracted from the total R-value.</li>
            <li>The single reflective vapour permeable membrane was assumed to have an e=0.87 and e=0.03.</li>
            <li>The double reflective vapour permeable membrane was assumed to have an e=0.03 and e=0.03.</li>
            <li>Bulk insulation and membranes must be suitable for intended use and be installed as per manufacturer's recommendations.</li>
            <li>Building Code of Australia (BCA) outlines some recommended thermal break materials with an R=value of 0.2.</li>
            <li>For Scyon Linea cladding the total R value may be increased by 0.1 due to the products thickness.</li>
        </ol>
		</div>
      	
	</div>
	
</div>

<footer id="home-footer">
    <?php include('menus/bottommenu.php');  ?>
	<?php include('panels/footer.php');  ?>
</footer>
</div>
</div>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-17010052-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script src="calc/jquery.cookie.js"></script>
<script>
  $('#login').click(function(){
    //alert('login');
    if ($('#remember').attr('checked')) {

      $.cookie('email', null);
      $.cookie('pass', null);
      $.cookie('remember', null);
      var email = $('#email').attr("value");
      var pass = $('#pass').attr("value");
      // set cookies to expire in 14 days
      $.cookie('email', email, { expires: 14 });
      $.cookie('pass', pass, { expires: 14 });
      $.cookie('remember', true, { expires: 14 });
      //alert(email+pass+"set");
    } else {
// reset cookies
      $.cookie('email', null);
      $.cookie('pass', null);
      $.cookie('remember', null);
      //alert(email+pass+"notset");
    }
    //false;
  });
 

  var remember = $.cookie('remember');
  if ( remember == 'true' ) {
    var email = $.cookie('email');
    var pass = $.cookie('pass');
// autofill the fields
    $('#email').attr("value", email);
    $('#pass').attr("value", pass);
    //alert(email+pass+"notset");
    $('#remember').attr('checked',true);
  }

  $(function() {
    var table = $('.jScrollPaneContainer #files_layer > table');  
    if (table.length > 0) {
      var scrollPane = $(table).parent().parent();
      var tableHeight = $(table).height();
      var scrollPaneHeight = $(scrollPane).height();
      if (tableHeight < scrollPaneHeight) {
        $(scrollPane).css('height', tableHeight + 'px');
      }
      $(table).parent().css('top', '0');
    }
  })();
  
</script>

</center>

</body>
</html>