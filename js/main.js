(function ($) {
	$(document).ready(function () {
		/*code*/

		$('.main-navigation ul li a').click(function () {
		
			$(this).parent().find('ul').show('fast');
		
		});
		$('.main-navigation ul li ul li a.close').click(function () {
		
			$(this).parent().parent().hide('fast');
		
		});
		$('.first-time-user h2 a').hover(function () {
			$(this).find('img.gr').animate({opacity:"show"}, 200);
		},
		function () {
			$(this).find('img.gr').animate({opacity:"hide"}, 200);
		});
		$('.guidance a').hover(function () {
			$('.guidance .disclaimer').fadeIn('fast');
		},
		function() {
			$('.guidance .disclaimer').fadeOut('fast');
		});
		
		$('.main-navigation ul li:first').addClass('first-child');
$('.dtc ul li').prepend('<span>&#149;</span>');

		/*Begin animation slider*/
		if (($('.js-slide1').size())>0) {
				$('.light-black-block').animate({height:"show"}, 500, function () {
					$('#slider-welcome .contact').animate({opacity:"show"}, 500);
					$('.icon-people-menu a').animate({opacity:"show"}, 500);
					reset_people();
					$('.light-black-block img').animate({opacity:"show"}, 500, function () {
						$('.blue-block').animate({height:"show"}, 500, function () {
							$('.welcome-text').animate({opacity:"show"}, 500);
						});
					});
				});
		}
		if (($('.js-slide2').size())>0) {
			$('.light-black-block').animate({height:"show"}, 900, function () {
				$('#slider-welcome .contact').animate({opacity:"show"}, 500);
				$('#f-navigation ul').animate({opacity:"show"}, 900);
				$('.light-black-block img').animate({height:"show"}, 500, function () {
					$('#resurse-navigation, .first-time-user, .form-post-code').animate({opacity:"show"}, 500);
					$('.transparent-block').animate({height:"show"}, 10, function () {
						$('.welcome-text').animate({opacity:"show"}, 500);
						$('.icon-new-img').animate({opacity:"show"}, 400);
						
					});
				});
			});
		}
		if (($('.js-slide3').size())>0) {
			$('.light-black-block').animate({height:"show"}, 900, function () {
				$('#slider-welcome .contact').animate({opacity:"show"}, 500);
				
				$('.light-black-block #swiffycontainer').animate({opacity:"show"}, 500, function () {
					$('#resurse-navigation, .first-time-user, .form-post-code').animate({opacity:"show"}, 500);
					$('#f-navigation ul').animate({opacity:"show"}, 900);
					$('.transparent-block').animate({height:"show"}, 10, function () {
						$('.welcome-text').animate({opacity:"show"}, 500);
						$('.icon-new-img').animate({opacity:"show"}, 400);
						
					});
				});
			});
		}
$('#f-navigation ul').animate({opacity:"show"}, 900);




		/*End animation slider*/

		/*BEGIN MAIN-MENU*/
		$(".distributor a").hover(function(){
			$(".icon-people-menu .wrp-img, .icon-people-menu img").stop();
			$('.distributor .wrp-img').animate({left:'-73px',width:'330px'},500);
			$('.distributor img').animate({width:'330px', marginTop:'6px'},500);
			$('.designer .wrp-img').animate({left:'25px', width:'264px'},500);
			$('.designer img').animate({width:'264px', marginTop:'12px'},500);
			$('.builder .wrp-img').animate({left:'-22px'},500);
			$('.builder img').animate({marginTop:'37px'},500);

		},function(){reset_people()}  );
		$(".designer a").hover(function(){
			$(".icon-people-menu .wrp-img, .icon-people-menu img").stop();
			$('.distributor .wrp-img').animate({left:'-58px',width:'306px'},500);
			$('.distributor img').animate({width:'306px', marginTop:'34px'},500);
			$('.designer .wrp-img').animate({left:'23px', width:'288px'},500);
			$('.designer img').animate({width:'288px', marginTop:'4px'},500);
			$('.builder .wrp-img').animate({left:'-12px', width:'217px'},500);
			$('.builder img').animate({width:'217px', marginTop:'44px'},500);


		},function(){reset_people()}  );
		$(".builder a").hover(function(){
			$(".icon-people-menu .wrp-img, .icon-people-menu img").stop();
			$('.distributor .wrp-img').animate({left:'-58px',width:'300px'},500);
			$('.distributor img').animate({width:'300px', marginTop:'42px'},500);
			$('.designer .wrp-img').animate({left:'30px',width:'266px'},500);
			$('.designer img').animate({width:'266px', marginTop:'20px'},500);
			$('.builder .wrp-img').animate({left:'-26px',width:'247px'},500);
			$('.builder img').animate({width:'247px', marginTop:'12px'},500);
		},function(){reset_people()} );

		function reset_people() {
			$(".icon-people-menu .wrp-img, .icon-people-menu img").stop();
			$('.distributor .wrp-img').animate({left:'-45px',width:'300px'},500);
			$('.distributor img').animate({width:'300px', marginTop:'28px'},500);
			$('.designer .wrp-img').animate({left:'54px',width:'251px'},500);
			$('.designer img').animate({width:'251px', marginTop:'20px'},500);
			$('.builder .wrp-img').animate({left:'-14px',width:'224px'},500);
			$('.builder img').animate({width:'224px', marginTop:'37px'},500);
		}

		/*END MAIN-MENU*/

		/*f-navigation*/
		$('#f-navigation li').each(function(){
		$(this).hover(function(){
			$(this).find('.f-items-top').animate({marginLeft:'-40px',width:'80px'},100);
			$(this).find('.f-items-bottom').animate({bottom:'-10px',width:'80px',marginLeft:'-40px'},100);
		},
		function () {
			$(this).find(".f-items-top, .f-items-bottom").stop();
			$(this).find('.f-items-top').animate({marginLeft:'-37px',width:'75px'},100);
			$(this).find('.f-items-bottom').animate({bottom:'0px',width:'75px',marginLeft:'-37px'},100);

		});
		});

		/*End f-navigation*/

		// $('.techlit-det > div').each( function() {
			// var h = $(this).height() / 2;
			// $(this).css('margin-top','-' + h + 'px');
		// });
		
		$('.techlit-choose .desman a').each( function() {
			var a = $(this).attr('class');
			$(this).click( function() {
				
				$('.techlit-det > div#' + a + '').fadeIn('fast').siblings('.techlit-det > div').fadeOut('fast');
				
			});
		
		});
$('.techlit-choose .prodman a').each( function() {
			var a = $(this).attr('class');
			$(this).click( function() {
				
				$('.techlit-det > div#' + a + '').fadeIn('fast').siblings('.techlit-det > div').fadeOut('fast');
			
			});
		
		});

		/*End code*/
	});
	
	
})(this.jQuery);
