/**
 * 
 */
var g_sel_pg = "";
var ANISPEED = 1000;

$(document).ready(function(){
	
	$(".column-right#main-r-val").css("display","block").animate(
			{"opacity":1,"top":"0px","height":"436px"},
			{ duration: ANISPEED, queue: true }
	);
	g_sel_pg = "#main-r-val";
	
	$('ul.r-vals li a').click(function(){
		var pg = $(this).attr("href");
	//	console.log("pg="+pg+", old="+g_sel_pg);

		$(g_sel_pg+".column-right").css("display","none").animate(
				{"opacity":0,"top":"0px","height":"0px"},
				{ duration: ANISPEED, queue: true }
		);;
		$(pg+".column-right").css("display","block").animate(
				{"opacity":1,"top":"0px","height":"436px"},
				{ duration: ANISPEED, queue: true }
		);
		g_sel_pg = pg;
		return false;
	});
	
	$('a.closebtn').click(function(){

		$(g_sel_pg+".column-right").css("display","none").animate(
				{"opacity":0,"top":"0px","height":"0px"},
				{ duration: ANISPEED, queue: true }
		);
		$(".column-right#main-r-val").css("display","block").animate(
				{"opacity":1,"top":"0px","height":"436px"},
				{ duration: ANISPEED, queue: true }
		);
		g_sel_pg = "#main-r-val";
		return false; 
	});
	
	$('a.importantbtn').click(function(){
		$(g_sel_pg+".column-right").css("display","none").animate(
				{"opacity":0,"top":"0px","height":"0px"},
				{ duration: ANISPEED, queue: true }
		);
		$(".column-right#pg_important").css("display","block").animate(
				{"opacity":1, "top":"0px","height":"436px"},
				{ duration: ANISPEED, queue: true }
		);
		g_sel_pg = "#pg_important";
		return false; 
	});
});
