$(document).ready(function() {	

	//select all the a tag with name equal to modal
	$('a[name=modal]').click(function(e) {
		//Cancel the link behavior
		
		e.preventDefault();
                //$("#dialog").append("<p>content1</p>");
				if($(this).attr('title') == 'history'){
				
				var full_id = $(this).attr('id');
				var n=full_id.split("_"); 
				hist_id = n[1];
				
$("div.inq_history").each(function () {
      if ($(this).attr("id") == 'hist_' + hist_id) {
          $(this).show();
        }
		else{
		$(this).hide();
		}
	 });
	 
	 var datastring = "type=makeReadText&id=" + hist_id;
	  $.ajax({
		type: "POST",
		url: "http://www.accel.com.au/downloadinterfaceajax.php",
 data: datastring,
   success: function(html){
   $("p#cont_" + hist_id).attr('class','viewed');
   }
   });

				}
				else{
                var inq_id = $(this).attr('id');
				$('input#inq_id').val(inq_id);
				
$("div.inq_files").each(function () {
       if ($(this).attr("id") == 'inq_' + inq_id) {
          $(this).show();
        }
		else{
		$(this).hide();
		}
     });
	  }

		
		//Get the A tag
		var id = $(this).attr('href');
	
		//Get the screen height and width
		var maskHeight = $(document).height();
		var maskWidth = $(window).width();
	
		//Set heigth and width to mask to fill up the whole screen
		$('#mask').css({'width':maskWidth,'height':maskHeight});
		
		//transition effect		
		$('#mask').fadeIn(1000);	
		$('#mask').fadeTo("slow",0.8);	
	
		//Get the window height and width
		var winH = $(window).height();
		var winW = $(window).width();
              
		//Set the popup window to center
		$(id).css('top',  winH/2-$(id).height()/2);
		$(id).css('left', winW/2-$(id).width()/2);
	
		//transition effect
		$(id).fadeIn(2000); 
	
	});
	
	//if close button is clicked
	$('.window .close').click(function (e) {
		//Cancel the link behavior
		e.preventDefault();
		
		$('#mask').hide();
		$('.window').hide();
	});		
	
	//if mask is clicked
	$('#mask').click(function () {
		$(this).hide();
		$('.window').hide();
	});			

	$(window).resize(function () {
	 
 		var box = $('#boxes .window');
 
        //Get the screen height and width
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();
      
        //Set height and width to mask to fill up the whole screen
        $('#mask').css({'width':maskWidth,'height':maskHeight});
               
        //Get the window height and width
        var winH = $(window).height();
        var winW = $(window).width();

        //Set the popup window to center
        box.css('top',  winH/2 - box.height()/2);
        box.css('left', winW/2 - box.width()/2);
	 
	});
	
});