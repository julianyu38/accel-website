jQuery(document).ready(function(){
  $(".sc_submitter").click(
          function() {
              var oForm = $("form[id=fire_wall_form]");
              if ($('#frame').val() == 0) { alert("Select a Frame"); return; }
              if ($('#minfire').val().indexOf("lease") == 1) { alert("Select Minimum Fire Rating"); return; }
              var str = $("#fire_wall_form").serialize();
                $.ajax({
                    type: "POST",
                    url: "./sc_fire_walls_two_results.php",
                    data: { data: str},
                    success: function(data){
                      if(data){
                        $('#sc_firewall_content').html(data);
                        $('#pic_box').hide();
                        $('.sc_bush_layers > div.sc_firewall_results').slideDown();
                      }
                    }
                  });
              }
  );

  $('a.sc_firewall_close').click(function() {
    $(this).parent().hide();
    $(this).parent().find('div.sc_bush_lsub').hide();
    $('.solutt div').hide();
    $('#pic_box').show();
  });
});