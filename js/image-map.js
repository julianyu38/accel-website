$('#imagemap area').each(
	function() {
		var areaName = $(this).attr('class');
		$(this).hover(
			function() {
				$('.imagemap-highlights img.' + areaName).show();
			},
			function() {
				$('.imagemap-highlights img.' + areaName).hide();
			}
		);
	}
);