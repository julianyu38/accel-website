/**
 * Created with JetBrains PhpStorm.
 * User: Alex
 * Date: 22.10.12
 * Time: 1:24
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function(){
    var floor = ['Ceiling Systems','2 x 13mm Boral FireSTOP plasterboard','13 + 16mm FireSTOP or FireShield plasterboard','2 x 16 Boral FireSTOP plasterboard','3 x 16 Boral FireSTOP plasterboard'];
    var tfcs = [
        {title:'1systems',data:['60/60/60','30min','61','55','57']},
        {title:'2systems',data:['60/60/60','60min','62','56','56']},
        {title:'3systems',data:['90/90/90','60min','61','54','57']},
        {title:'4systems',data:['120/120/120','90min','62','56','55']}];
    var cfcs = [
        {title:'1systems',data:['60/60/60','30min','60','55','36']},
        {title:'2systems',data:['60/60/60','60min','61','55','35']},
        {title:'3systems',data:['90/90/90','60min','60','53','35']},
        {title:'4systems',data:['120/120/120','90min','61','55','34']}];
    $('#floor_finish').change(function(){
        if(($('#floor_finish option:selected').hasClass('tf'))||($('#floor_finish option:selected').hasClass('cf'))){
            $('#ceiling_system').empty();
            if($('#floor_finish option:selected').hasClass('tf')){$('#ceiling_system').removeClass('cf').removeClass('tf').addClass('tf')};
            if($('#floor_finish option:selected').hasClass('cf')){$('#ceiling_system').removeClass('cf').removeClass('tf').addClass('cf')};
            for(var i=0;i<floor.length;i++){
                var str = '<option class=\''+i+'systems\''+'>'+floor[i]+'</option>';
                $('#ceiling_system').append(str);
            }
        }
    });
    $('#ceiling_system').change(function(){
        if($(this).hasClass('tf')){
            if(!$('#ceiling_system option:selected').hasClass('0systems')){
                $('.level_values').css('display','block');
                var chOp = $('#ceiling_system option:selected').attr('class');
                var result= $.grep(tfcs, function(elem){ return elem.title ==chOp });
                $('.fire_level .size').text(result[0].data[0]);
                $('.fire_level .info').text('Below RISF '+result[0].data[1]);
                $('.s1').text(result[0].data[2]);
                $('.s2').text(result[0].data[3]);
                $('.s3').text(result[0].data[4]);
            }
        }
        if($(this).hasClass('cf')){
            if(!$('#ceiling_system option:selected').hasClass('0systems')){
                $('.level_values').css('display','block');
                var chOp = $('#ceiling_system option:selected').attr('class');
                var result= $.grep(cfcs, function(elem){ return elem.title ==chOp });
                $('.fire_level .size').text(result[0].data[0]);
                $('.fire_level .info').text('Below RISF '+result[0].data[1]);
                $('.s1').text(result[0].data[2]);
                $('.s2').text(result[0].data[3]);
                $('.s3').text(result[0].data[4]);
            }
        }
    })
})

