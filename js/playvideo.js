(function() {
  $('#audio1').hide();

  $('.links a').click(function(event) {
    event.preventDefault();
    var href = $(this).attr('href');
    if ($(this).hasClass('mp3')) {
      stopVideo();
      hideVideo();
      showAudio();
      playAudio(href);      
    } else {
      stopAudio();
      hideAudio();
      showVideo();
      playVideo(href);
    }
    changeHeader(this);
  });

  function changeHeader(selectedLink) {    
    var header = $(selectedLink).children('h3').text();
    $('#playing').text(header);
  }

  function showAudio() {
    $('#audio1').show();
  }

  function hideAudio() {
    $('#audio1').hide();
  }

  function stopAudio() {
    var audioElement = $('#audio1')[0];
    if (audioElement.pause) {
      audioElement.pause();
    }
  }

  function playAudio(href) {    
    var audioElement = $('#audio1')[0];

    if (supports_media('audio/mpeg', audioElement)) {
      audioElement.src = href + '.mp3';
      audioElement.load();
      audioElement.play();
    } else if (supports_media('audio/ogg', audioElement)) {
      audioElement.src = href + '.ogg';
      audioElement.load();
      audioElement.play();
    } else {
      hideAudio();
      var fileName = 'video/src/video/' + href.slice(href.lastIndexOf('\/') + 1) + '.mp3';
      $('object').remove();
      $('#video1').before($('<object id="ie_video" type="application/x-shockwave-flash" data="http://flv-mp3.com/i/pic/ump3player_500x70.swf" height="77" width="520"><param name="wmode" value="transparent" /><param name="allowFullScreen" value="true" /><param name="allowScriptAccess" value="always" /><param name="movie" value="http://flv-mp3.com/i/pic/ump3player_500x70.swf" /><param name="FlashVars" value="way=http://www.accel.com.au/video/src/video/JH_Base_Coat_Podcast_Aug09.mp3&amp;swf=http://flv-mp3.com/i/pic/ump3player_500x70.swf&amp;w=520&amp;h=77&amp;time_seconds=0&amp;autoplay=1&amp;q=1&amp;skin=black&amp;volume=50&amp;comment=" /></object>'));
      $('#ie_video').css('margin-top', '315px');
    }
  }

  function showVideo() {
    $('#video1').show();
  }

  function hideVideo() {
    $('#video1').hide();
  }

  function stopVideo() {
    var videoElement = $('#video1')[0];
    if (videoElement.pause) {
      videoElement.pause();
    }
  }

  function playVideo(href) {
    var videoElement = $('#video1')[0];

    if (supports_media('video/mp4', videoElement)) {
      videoElement.src = href + '.mp4';
      videoElement.load();
      videoElement.play();      
    } else if (supports_media('video/ogg', videoElement)) {
      videoElement.src = href + '.ogv';
      videoElement.load();
      videoElement.play();      
    } else {
      var fileName = 'video/src/video/' + href.slice(href.lastIndexOf('\/') + 1) + '.flv';
      $('object').remove();
      $('#video1').before($('<object id="ie_video" type="application/x-shockwave-flash" data="http://flv-mp3.com/i/pic/uflvplayer_500x375.swf" height="390" width="520"><param name="bgcolor" value="#dddddd" /><param name="allowFullScreen" value="true" /><param name="allowScriptAccess" value="always" /><param name="movie" value="http://flv-mp3.com/i/pic/uflvplayer_500x375.swf" /><param name="FlashVars" value="way=http://www.accel.com.au/' + fileName + '&amp;swf=http://flv-mp3.com/i/pic/uflvplayer_500x375.swf&amp;w=520&amp;h=390&amp;pic=http://&amp;autoplay=1&amp;tools=1&amp;skin=black&amp;volume=50&amp;q=1&amp;comment=" /></object> '));
    }  
  }

  function supports_media(mimetype, elem) {
    if(typeof elem.canPlayType == 'function') {
      var playable = elem.canPlayType(mimetype);
      if((playable.toLowerCase() == 'maybe')||(playable.toLowerCase() == 'probably')){
        return true;
      }
    }
    return false;
  }
})();