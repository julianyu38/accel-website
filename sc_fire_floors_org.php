<?php //include('mustlog.php');  ?>
<!DOCTYPE html>
<!--[if IE 8 ]>    <html lang="ru" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="ru" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="ru" class="no-js"> <!--<![endif]-->
<title>ACCEL | JH ACCESS SOLUTIONS CENTRE | JAMES HARDIE</title>
<meta charset="UTF-8">
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/basex.css">

  <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/themes/base/jquery-ui.css" type="text/css" media="all" />
  <link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" />
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js" type="text/javascript"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="js/jquery.scrollTo-min.js"></script>
  <script src="js/our-jqueryx.js" type="text/javascript"></script>
  <script type="text/javascript" src="js/mainx.js"></script>
  <script type="text/javascript" src="js/modernizr.js"></script>
  <!--[if IE]>  <link rel="stylesheet" href="css/iex.css"><![endif]-->

  <!-- TABS -->
  <link type="text/css" href="css/jquery-ui-1.8.24.custom.css" rel="stylesheet">
  <link rel="stylesheet" href="css/cadassist.css">
  <script type="text/javascript" src="js/jquery-ui-1.8.24.custom.min.js"></script>
  <script type="text/javascript" src=js/fire_floors_select.js></script>
  <script type="text/javascript">
      $(function() {
        $('#tabs').tabs();
      });
  </script>
  <!-- -->

  <link rel="shortcut icon" href="favicon.ico">
  <link rel="icon" type="image/ico" href="favicon.ico">  
  <script type="text/javascript">
  <!--
  function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
  }
  function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
  var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
  if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
  }
  function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
	d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);
	}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
	//-->
	</script>
  <script type="text/javascript" src="/js/jquery.mousewheel.min.js"></script>
  <script type="text/javascript" src="/js/jScrollPane.js"></script>
  <link rel="stylesheet" type="text/css" media="all" href="/js/jScrollPane.css" />
</head>

<body>
<center>

<div id="page">


<div id="wrapper2">
<img src="images/leftshadow.jpg" alt="" class="leftshadow" border="0" width="6" />
<img src="images/bottomshadow.jpg" alt="" class="bottomshadow" border="0" height="7" />

  <!-- BEGIN #HEADER -->
  <header id="main-header">
   
    <?php include('panels/logo.php');  ?>
	<?php include('panels/memberlogin.php');  ?>
  </header>
  <!-- END #HEADER -->


  <div class="js-slide2">
    <div class="top-bl all-menu">
       <?php include('menus/topmenu.php');  ?>
    </div>
  </div>

<div class="mainwrappbig sc_main">
	<div class="sc_mainnav">
	<?php include('menus/sc_leftnav.php');  ?>
	</div>

  <div class="column-right">
    <img class="main-image" src="images/sc_floors_main.jpg" alt="Floors image" width="550" height="423">
    <img class="blank-image" src="images/sc_floors_mainimgblank.png" alt="Blank image" usemap="#map" width="550" height="423">
    <div class="imagemap-highlights">
      <img class="sc_floors_bulk_insulation" src="images/sc_floors_bulk_insulation.png" alt="R 2.0 bulk insulation" width="550" height="423">
      <img class="sc_floors_fire_rated_plasterboard" src="images/sc_floors_fire_rated_plasterboard.png" alt="Fire Rated Plasterboard" width="550" height="423">
      <img class="sc_floors_furring_channel" src="images/sc_floors_furring_channel.png" alt="Furring channel" width="550" height="423">
      <img class="sc_floors_interior_flooring" src="images/sc_floors_interior_flooring.png" alt="19mm ScyonTM SecuraTM interior flooring" width="550" height="423">      
      <img class="sc_floors_timber_floor_joists" src="images/sc_floors_timber_floor_joists.png" alt="Timber Floor Joists" width="550" height="423">
    </div>
    <map id="imagemap" name="map">
      <area class="sc_floors_interior_flooring" href="javascript:void(0)" shape="poly" coords="4,5,5,141,15,139,28,137,44,138,60,140,78,142,90,141,99,139,112,135,129,126,142,119,159,117,177,117,205,119,237,122,266,121,285,119,311,112,332,105,345,101,362,93,393,76,432,52,453,38,476,23,498,9,509,1" onclick="blur()" onfocus="navigator.appName == 'Microsoft Internet Explorer' ? blur() : null">
      <area class="sc_floors_timber_floor_joists" href="javascript:void(0)" shape="poly" coords="1,275,82,322,87,318,89,287,2,238,2,244,1,210,138,290,146,285,146,256,2,173,2,179,29,162,195,258,201,254,202,222,44,134,17,135,31,141,32,158,32,158,347,148,365,158,372,154,372,124,331,101,321,104,310,107,345,128,347,133,347,145,347,148,307,190,313,187,313,154,245,120,229,118,211,116,289,160,292,182,306,191,250,225,257,220,255,194,255,189,136,121,129,116,116,125,157,148,154,167,154,167" onclick="blur()" onfocus="navigator.appName == 'Microsoft Internet Explorer' ? blur() : null"/>
      <area class="sc_floors_bulk_insulation" href="javascript:void(0)" shape="poly" coords="5,173,30,159,30,144,18,136,7,136,6,150,128,180,154,166,153,147,117,125,99,134,90,138,72,138,43,135,108,168,108,168,257,201,291,182,290,164,213,118,191,114,159,113,147,114,132,117,257,190,257,193,313,168,348,149,347,131,313,108,300,110,274,116,246,119,312,157,312,159" onclick="blur()" onfocus="navigator.appName == 'Microsoft Internet Explorer' ? blur() : null"/>
      <area class="sc_floors_fire_rated_plasterboard" href="javascript:void(0)" shape="poly" coords="2,236,31,253,51,241,7,214,4,227,173,180,237,217,202,236,201,222,152,193,165,185,117,213,181,249,146,270,146,256,96,225,116,214,52,176,107,206,86,219,32,188,48,179,60,245,124,282,90,302,89,287,40,258,52,249,2,276,82,322,415,128,431,135,48,355,4,327,4,305" onclick="blur()" onfocus="navigator.appName == 'Microsoft Internet Explorer' ? blur() : null"/>
      <area class="sc_floors_furring_channel" href="javascript:void(0)" shape="poly" coords="38,258,60,244,54,241,31,254,33,255,86,221,94,226,113,214,117,212,111,207,86,222,95,226,141,188,150,194,171,179,166,175,144,186,89,313,138,283,126,283,89,305,89,308,145,280,189,253,181,249,145,271,146,271,201,248,246,221,238,217,201,239,201,239,257,215,303,189,295,184,258,205,257,207,314,182,360,155,353,150,313,173,314,176" onclick="blur()" onfocus="navigator.appName == 'Microsoft Internet Explorer' ? blur() : null"/>
    </map>
      <div class="textareaWP">
          <h1>Floor Systems: Fire &amp; Acoustic</h1>
          <div class="yellowLabel">Use the options below to calculate your floor systems <br/> fire and acoustic performance</div>
          <div class="aWrapper">
              <form action="">
                  <div class="lbWP">
                      <label for="" class="ch_floor_finish">Choose Floor Finish</label>
                      <span class="seporator">/</span>
                      <label for="" class="ch_ceiling_system">Choose Ceiling System</label>
                  </div>
                  <div class="slWP">
                      <select name="" id="floor_finish" class="ch_floor_finish">
                          <option value="">Floor Finishes</option>
                          <option value="" class="tf">Tiled Floor</option>
                          <option value="" class="cf">Carpeted Floor</option>
                      </select>
                      <span class="seporator">/</span>
                      <select name="" id="ceiling_system" class="ch_ceiling_system">

                      </select>
                  </div>
              </form>
              <div class="note">
                  <h3>note:</h3>
                  <p>Ceiling systems are all furred on acoustic mounts</p>
              </div>
              <div class="level_values">
                  <div class="label">
                      <div class="imgLB"></div>
                      <div class="textLB">
                          <span class="fire_level">Fire Rated Level (FRL)</span>
                          <span class="rw">Rw</span>
                          <span class="rwctr">Rw + Ctr</span>
                          <span class="lnwcl">Ln<sub>1</sub>w + Cl</span>
                      </div>
                  </div>
                  <ul>
                      <li class="fire_level">
                          <span class="size"></span>
                          <span class="info"></span>
                      </li>
                      <li class="s1"></li>
                      <li class="s2"></li>
                      <li class="s3 last"></li>
                  </ul>
              </div>
              <h3>important note:</h3>
              <p>
                The fire rated floor systems are from the underside only. <br/>
                e.g. one way only
              </p>
              <div class="actionWP">
                  <ul>
                      <li><span>Request Fire &amp; <br/> Acoustic Certificate</span><a href="#">Request</a></li>
                      <li class='cEl'><span>Download Technical <br/> Supplement</span><a href="#">Download</a></li>
                      <li><span>Show me<br/>Construction Details</span><a href="#">Show Me</a></li>
                  </ul>
              </div>
          </div>
      </div>
  </div>
</div>

<footer id="home-footer">
    <?php include('menus/bottommenu.php');  ?>
	<?php include('panels/footer.php');  ?>
</footer>
</div>
</div>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-17010052-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script src="calc/jquery.cookie.js"></script>
<script>
  $('#login').click(function(){
    //alert('login');
    if ($('#remember').attr('checked')) {

      $.cookie('email', null);
      $.cookie('pass', null);
      $.cookie('remember', null);
      var email = $('#email').attr("value");
      var pass = $('#pass').attr("value");
      // set cookies to expire in 14 days
      $.cookie('email', email, { expires: 14 });
      $.cookie('pass', pass, { expires: 14 });
      $.cookie('remember', true, { expires: 14 });
      //alert(email+pass+"set");
    } else {
// reset cookies
      $.cookie('email', null);
      $.cookie('pass', null);
      $.cookie('remember', null);
      //alert(email+pass+"notset");
    }
    //false;
  });
 

  var remember = $.cookie('remember');
  if ( remember == 'true' ) {
    var email = $.cookie('email');
    var pass = $.cookie('pass');
// autofill the fields
    $('#email').attr("value", email);
    $('#pass').attr("value", pass);
    //alert(email+pass+"notset");
    $('#remember').attr('checked',true);
  }

  $(function() {
    var table = $('.jScrollPaneContainer #files_layer > table');  
    if (table.length > 0) {
      var scrollPane = $(table).parent().parent();
      var tableHeight = $(table).height();
      var scrollPaneHeight = $(scrollPane).height();
      if (tableHeight < scrollPaneHeight) {
        $(scrollPane).css('height', tableHeight + 'px');
      }
      $(table).parent().css('top', '0');
    }
  })();
  
</script>

</center>
  <script type="text/javascript" src="js/image-map.js"></script>
</body>
</html>